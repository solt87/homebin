#! /bin/sh -
## Limit ~/.xsession-errors in size.
## Useful as a cron job.
##
## DEPENDENCIES:
## - `sponge` from the "moreutils" package

## Copyright 2020-2022, 2024 Solt Budavári

## SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck shell=sh
set -eu

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.


FILE="$HOME/.xsession-errors"
MAX_LINES=10000

LINES=$( wc -l "${FILE}" | cut -d" " -f1 )

if [ "${LINES}" -gt "${MAX_LINES}" ]; then
    tail -n "${MAX_LINES}" "${FILE}" | sponge "${FILE}"
fi

exit $?
