#! /usr/bin/python3
## Download the specified currency exchange rates and
## save the data to the specified file.

## Copyright 2019, 2022 Solt Budavári

## SPDX-License-Identifier: GPL-3.0-or-later

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

## The following is a sample config file for this script;
## adapt it to your needs.
##
## --- # Api key and settings for Open Exchange Rates currency script
## api_key: 0123456789abcdef
## base_url: https://openexchangerates.org/api/latest.json
## wanted_currencies: [ AUD, CAD, EUR, GBP ]
## target_file: ~/Documents/currency_rates.csv
## ...


import csv
import json
import os.path
import time

import requests
import yaml



def main():
    
    SETTINGS_FILE = os.path.expanduser( "~/etc/get_exchange_rates.yaml" )
    wanted_base = "HUF"
    
    ## Get the needed arguments from the given YAML file
    
    with open( SETTINGS_FILE, "r" ) as sf:
        args = yaml.load( sf, Loader = yaml.SafeLoader )
    
    API_KEY = args[ "api_key" ]
    BASE_URL = args[ "base_url" ]
    WANTED_CURRENCIES = ",".join( args[ "wanted_currencies" ] )
    TARGET_FILE = os.path.expanduser( args[ "target_file" ] )
    
    ## Download the exchange rate for the desired currencies
    currency_data = download_currency_data( BASE_URL
                                            + "?app_id=" + API_KEY
                                            + "&symbols=" + WANTED_CURRENCIES )
    
    ## The base currency of the downloaded dataset
    BASE_CURRENCY = currency_data[ "base" ]
    
    ## Assemble the row (a tuple) that we will write to file
    row = create_row( currency_data, wanted_base, BASE_CURRENCY )
    
    ## Write all the exchange data (the row) to file
    write_row_to_file( TARGET_FILE, row )

    return None


def download_currency_data( url ):
    req = requests.get( url )
    
    if req.status_code == requests.codes.ok:
        result = json.loads( req.text )
    else:
        req.raise_for_status()
    
    return result


def create_row( data, wanted_base, base ):
    ## Adjust the exchange rates to the wanted base currency, and
    ## convert the Unix-time timestamp to human-readable format
    
    rates = {}
    
    for currency in data[ "rates" ]:
        if currency != wanted_base:
            rates[ currency ] = ( data[ "rates" ][ wanted_base ]
                                  / data[ "rates" ][ currency ] )
        else:
            rates[ base ] = ( data[ "rates" ][ currency ] )
    
    raw_time = data[ "timestamp" ]
    nice_time = time.strftime( "%Y.%m.%d. %H:%M:%S",
                               time.localtime( raw_time ) )
    
    return tuple( [ raw_time, nice_time ]
                  + [ rates[ currency ] \
                      for currency in sorted( rates.keys() ) ] )


def write_row_to_file( file, row ):
    with open( file, "a", newline = "\n" ) as csvfile:
        csv_writer = csv.writer(
            csvfile,
            dialect = "unix",
            delimiter = ";",
            quotechar = '"',
            quoting = csv.QUOTE_MINIMAL
        )
        
        csv_writer.writerow( row )
    
    return None


if __name__ == "__main__":
    main()
