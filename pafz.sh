#! /bin/sh -
## Use `fzf` with `apt search` and `flatpak search` to
##   preview search results.

## Copyright 2023, 2024 Solt Budavári

## SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck shell=sh
set -eu

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

PROGNAME=$( basename "$0" )

main ()
{
    if [ $# -lt 2 ]; then
        exit_usage
    fi
    
    case $1 in
        "-a")
            shift 1
            apt-cache search "$@" | \
              fzf --reverse \
                  --preview "apt show {1} 2>/dev/null" \
                  --bind "ctrl-i:execute(sudo apt install {1})"
        ;;
    
        "-f")
            shift 1
            flatpak search "$@" | \
              fzf -d '\t' \
                  --reverse \
                  --preview "flatpak remote-info {6} {3}" \
                  --bind "ctrl-i:execute(flatpak install {6} {3})"
        ;;
    
        *) exit_usage ;;
    esac
    
    exit $?
}

exit_usage ()
{
    cat 1>&2 <<- EOF
	Usage: $PROGNAME {-a|-f} [search options] SEARCH_TERM

	Options:
	 -a        search with 'apt-cache search'
	 -f        search with 'flatpak search'

	[search_options]    options to pass to the search command
	EOF
    exit 1
}

main "$@"
