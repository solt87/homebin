#!/bin/sh -
## Sort a file and save the result into the original file.
## NOTE: this program is obsolete, because `sort` _can_
## write its results into the original file with `-o`. See `man 1 sort`.

# Copyright 2016, 2017, 2019 Solt Budavári

# SPDX-License-Identifier: GPL-3.0-or-later

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# We need the name of the file to sort
if [ $# -ne 1 ] ; then
    echo "Usage: abc [filename]" 1>&2
    exit 1
fi

# Create a temporary file
TMPFILE=$( mktemp  /tmp/abcXXXXX )

# Sort the file into the temporary file
sort "$1" > "$TMPFILE"

# Replace the original file with the temp file
mv -v "$TMPFILE" "$1"

exit $?

