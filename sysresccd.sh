#! /bin/sh -
## Check if a newer version of SystemRescueCD is available, if so, open download page.

# Copyright 2016, 2019, 2022, 2024 Solt Budavári

# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck shell=sh
set -eu

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

## The name of the thing the version of which we check:
NAME="SystemRescueCD"

## The url of the page we want to check
CHECKURL=http://www.sysresccd.org/Changes-x86

## The version we have
HAVE_VER="5.3.2"

## Temporary file for the downloaded html
TMPHTML=$( mktemp -t sysresccdXXXX )

## Fetch the target page quietly
wget "$CHECKURL" -q -O "$TMPHTML"

## Get the first line (newest comes first) that
## has a version (digit, dot, digit, dot, digit) in it and
## convert tabs to spaces, remove asterisks and remove leading white-space.
CUR_TXT=$( html2text "$TMPHTML" | grep "[[:digit:]]\.[[:digit:]]\.[[:digit:]])" | head -1 | \
           sed -e 's/\t/ /g' -e 's/\*//g' -e 's/^[ \t]+//g' )

## Separate the version string and the date string
CUR_VER=$( echo "$CUR_TXT" | awk '{print $1}' | sed -e 's/[^0-9.]//g' )
#CUR_DATE=$( echo $CUR_TXT | awk '{print $2}' | sed -e 's/-/. /g' -e 's/[^0-9. ]//g' )

#echo $CUR_VER $CUR_DATE  ## For debugging

## Compare have-version with avaiable version and report if they are different
if [ "$HAVE_VER" != "$CUR_VER" ] ; then
    x-www-browser "$CHECKURL" &
fi

## Remove temporary file
rm "$TMPHTML"

exit $?
