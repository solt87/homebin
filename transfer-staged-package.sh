#!/bin/sh -e
#
# Forked from https://gist.github.com/ruario/a188c688209148f1f947
#

# Copyright 2017, 2019, 2024 Solt Budavári
# NOTE: the original work (referenced above) does not contain a
# copyright notice, so I can't include that.

# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck shell=sh
set -eu

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Function to display usage message
helptext() {
  cat << HELP

Usage: $0 [option]... packagename

Options:

  -h,  --help                  (Show this help text)
  -a,  --archive               (Create an archive, instead of install)
  -nm, --no-man-compression    (Do not compress man pages)
  -ns, --no-strip-binaries     (Do not strip binaries)
  -no, --no-reset-ownership    (Do not set file ownership to root)
  -np, --no-reset-permissions  (Do not try and fix file permissions)

HELP
}

# Set default parameters
archive=n
doman=y
dostrip=y
doroot=y
doperms=y

# Parse command-line arguments
while [ 0 ]; do
  if [ "$1" = "-a" -o "$1" = "--archive" ]; then
    archive=y
    shift 1
  elif [ "$1" = "-nm" -o "$1" = "--no-man-compression" ]; then
    doman=n
    shift 1
  elif [ "$1" = "-ns" -o "$1" = "--no-strip-binaries" ]; then
    dostrip=n
    shift 1
  elif [ "$1" = "-no" -o "$1" = "--no-reset-ownership" ]; then
    doroot=n
    shift 1
  elif [ "$1" = "-np" -o "$1" = "--no-reset-permissions" ]; then
    doperms=n
    shift 1
  else
    break
  fi
done

if [ -z "$1" ]; then
  echo "Provide the package name as the first argument of the script" >&2
  helptext
  exit 1
elif [ "$1" = "-h" -o "$1" = "--help" ]; then
  helptext
  exit 0
fi

# Prepare the package according to parameters
if [ "$doman" = "y" ]; then
  for m in $(find . -regextype posix-egrep -regex '.*/man/([[:alnum:]\._-]*/)?man[[:alnum:]]*/.*' -type f | grep -vE '\.(gz|bz2|xz|lzma)$'); do
    gzip -9 $m
  done
  for l in $(find . -regextype posix-egrep -regex '.*/man/([[:alnum:]\._-]*/)?man[[:alnum:]]*/.*' -type l | grep -vE '\.(gz|bz2|xz|lzma)$'); do
    b=$(basename "$l")
    ( cd "$(dirname $l)"; ln -s $(readlink $b).gz $b.gz; rm $b )
  done
fi
if [ "$dostrip" = "y" ]; then
  for e in $(find . -type f -exec file {} \; | sed -rn 's/(.*): (setuid )*ELF.*(executable|object).*not stripped/\1/p'); do
    strip --strip-unneeded "$e"
  done
fi
if [ "$doroot" = "y" ]; then
  chown -R root:root *
fi
if [ "$doperms" = "y" ]; then
  chmod -R u+w,go+r-w,a+X-s *
fi

# Prepare the uninstall script of the package
mkdir -p usr/local/bin

cat <<EOF> "usr/local/bin/uninstall-$1"
#!/bin/sh -e
while read f; do
  if [ -e "\$f" -o -h "\$f" ]; then
    if [ -d "\$f" ]; then
      if ! ls -A "\$f" | grep -q ^; then
        if [ ! -h "\$f" ]; then
          rmdir -v "\$f"
        fi
      fi
    else
      rm -v "\$f"
    fi
  fi
done << FILE_LIST
EOF

find . -depth | sed -n 's,^\./,/,p' | grep -vxE "/usr/local/bin/uninstall-$1|/(etc|lib(32|64)?|opt|s?bin|usr((/local)?(/doc|/include|/info|/lib(32|64)?|/man(/man[1-9n])?|/s?bin|/share(/doc|/info|/man(/man[1-9n])?)?|/src)?)?|var)" >> "usr/local/bin/uninstall-$1"
printf "/usr/local/bin/uninstall-$1\nFILE_LIST" >> "usr/local/bin/uninstall-$1"
chmod 755 "usr/local/bin/uninstall-$1"

# Create an archive or transfer the package
if [ "$archive" = "y" ]; then
  echo "Creating an installable, backup archive: \"/var/tmp/$1.bin.tar.gz\""
  find . ! -type d | sed 's,^\./,,' | tar czf /var/tmp/$1.bin.tar.gz -T-
  printf "\nTo install, issue the following as root (or prefaced with 'sudo'):\n\n  tar xf $1.bin.tar.gz -C /\n\n"
else
  printf "Installing the following files:\n\n"
  find . ! -type d | cpio -pduv / | sed 's/^\.//'
  printf "\nTo uninstall run the following command as root (or prefaced with 'sudo'):\n\n  /usr/local/bin/uninstall-$1\n\n"
fi
