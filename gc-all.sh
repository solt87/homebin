#!/bin/sh --
## Enter into all directories, and do a git gc.

# Copyright 2017-2022, 2024 Solt Budavári

# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck shell=sh
set -eu

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

for dirname in $(find ./ -maxdepth 1 -type d ! -iname ".*" -print); do
    if cd "$dirname" ; then
        if [ -d .git ]; then
            printf "\n\n-= %s =-\n\n" "$dirname"
            ## Avoid letting the script abort due to
            ##  potential failure of git commands.
            set +e
            git gc
            set -e
        fi
        cd ..
    fi
done

exit $?
