#! /bin/sh -
# Perform some cleaning tasks

## Copyright 2021-2024 Solt Budavári

## SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck shell=sh
set -eu

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

printf "\n-= Removing unneeded package dependencies... =-\n\n"
sudo apt autoremove

printf "\n-= Cleaning old deb packages... =-\n\n"
sudo apt autoclean

if python3 -m pip 1>/dev/null 2>&1 && \
   [ -n "$( python3 -m pip freeze --user 2>/dev/null )" ] ; then
    printf "\\n-= Cleaning pip cache... =-\\n\\n"
    python3 -m pip cache purge
fi

if command -v gem 1>/dev/null 2>&1 ; then
    printf "\n\n-= Cleaning up old versions of %s's installed Ruby gems... =-\n\n" "${USER}"
    gem cleanup --user-install
fi

if command -v flatpak 1>/dev/null 2>&1 ; then
    printf "\n\n-= Removing unneeded flatpak packages... =-\n\n"
    flatpak uninstall --unused --delete-data
fi

exit $?
