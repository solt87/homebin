#!/bin/sh -
## Download the latest version of the chosen hosts blocklist and update /etc/hosts with it.

# Copyright 2016, 2018, 2019 Solt Budavári

# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck shell=sh
set -eu

# NOTE: I consider this script obsolete, as I now use
# Steven Black's hosts file generator instead.
# You can find it here: https://github.com/StevenBlack/hosts

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

## The system's hosts file
HOSTS=/etc/hosts

## The saved original hosts file
ORIG_HOSTS=$HOME/etc/hosts

## The URL of the hosts file to download
FILE_URL=http://someonewhocares.org/hosts/zero/hosts

## Temporary file to hold the downloaded hosts file
TMP_HOSTS=$( mktemp -t hostsXXXX )

## Clue to the line describing the version of the hosts block file
CLUE="Last updated"


## Download the new hosts file and check if its newer than what we have
wget -v "$FILE_URL" -O "$TMP_HOSTS"

CUR=$( grep "$CLUE" "$HOSTS" )
NEW=$( grep "$CLUE" "$TMP_HOSTS" )
if [ "$CUR" = "$NEW" ] ; then
    ## The downloaded hosts file is the same we have, so clean up and exit
    rm -v "$TMP_HOSTS"
    exit 0
fi

## Create the new, combined hosts file
NEW_HOSTS=$( mktemp -t newhostsXXXX )
cat "$ORIG_HOSTS" > "$NEW_HOSTS"
cat >> "$NEW_HOSTS" <<EndOfSeparator

## --- Dan Pollock's hosts file follows: --- ##

EndOfSeparator

cat "$TMP_HOSTS" >> "$NEW_HOSTS"

## Move the new hosts file to /etc/hosts, adjust it's right+ownership
sudo su -c "mv -v $NEW_HOSTS $HOSTS"
sudo su -c "chmod -v 644 $HOSTS"
sudo su -c "chown -v root:root $HOSTS"

## Clean up
rm -v "$TMP_HOSTS"

exit $?
