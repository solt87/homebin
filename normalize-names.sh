#! /bin/bash --
## Remove spaces, brackets and curly braces from
## filenames.

# Copyright 2018, 2019, 2022, 2023, 2024 Solt Budavári

# SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck shell=bash
set -euo pipefail

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

for item in *
do
        [ ! -f "$item" ] && continue
#        echo $item
        oldname=$( echo "$item" | sed -e 's;^\./;;' )
#        echo $oldname
        newname=$( echo "$oldname" | tr -s "[]{}' " _ | sed -e s/^_// )
#        echo $newname
        mv -v "${item}" \
              $( echo "$item" | \
                sed -e 's;^\./;;' | tr -s "[]{}' " _ | sed -e s/^_// -e s/_-_/__/g )
done

exit $?
