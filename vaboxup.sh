#! /bin/sh -
## Update any outdated vagrant boxes

## Copyright 2023-2024 Solt Budavári

## SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck shell=sh
set -eu

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

if ! command -v vagrant 1>/dev/null 2>&1 ; then
    echo "$0: Warning: command 'vagrant' not found, exiting." 1>&2
    exit 0
fi

vagrant box outdated --global | \
                     grep -F "is outdated" | \
                     sed -e 's/^\* *//' \
                         -e 's/\(^.*\) is outdated.*$/\1/' \
                         -e 's/ for//' | \
                     tr -d "'" | \
  while IFS=" " read -r box provider
      do
          vagrant box update --box "$box" --provider "$provider"
      done

exit $?
