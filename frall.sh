#! /bin/sh -
# Perform some update/upgrade tasks.

## Copyright 2021-2022, 2024 Solt Budavári

## SPDX-License-Identifier: GPL-3.0-or-later

# shellcheck shell=sh
set -eu

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <https://www.gnu.org/licenses/>.

printf "\n-= Updating distribution packages... =-\n\n"
sudo apt update && sudo apt full-upgrade

if python3 -m pip 1>/dev/null 2>&1 && \
   [ -n "$( python3 -m pip freeze --user 2>/dev/null )" ] ; then
    printf "\n\n-= Updating %s's pip packages... =-\n\n" "${USER}"
    python3 -m pip freeze --user | cut -d'=' -f1 | xargs python3 -m pip install --upgrade --user
fi

if command -v gem 1>/dev/null 2>&1 ; then
    printf "\n\n-= Updating %s's Ruby gems... =-\n\n" "${USER}"
    gem update --user-install
fi

if command -v flatpak 1>/dev/null 2>&1 ; then
    printf "\n\n-= Updating flatpak packages... =-\n\n"
    flatpak update
fi

exit $?
